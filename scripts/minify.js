var minify = require("node-minify-all/minify-all-api");

var opts = {
  rootpath: "dist",
  mode: "all",
  backups: false,
};

minify.process(opts);

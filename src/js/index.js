import "regenerator-runtime/runtime";

const timeout = (ms) => {
  return new Promise((resolve) => setTimeout(resolve, ms));
};

const start = async () => {
  const slides = document.querySelectorAll(".slide");
  for (let i = 0; i < slides.length; ++i) {
    slides[i].classList.add("active");

    if (i < slides.length - 1) {
      let delay = 2000;
      if (i === 2 || i === 3) {
        delay = 3000;
      }
      if (i === 6) {
        delay = 1000;
      }
      //console.log(i, delay);
      await timeout(delay);
      slides[i].classList.remove("active");
    }
  }
};

start();
